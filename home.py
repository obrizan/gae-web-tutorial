# coding=utf8
"""
Created on 25.01.2015

@author: Vladimir Obrizan
"""

from main_page_handler import *


app = webapp2.WSGIApplication([
    ('/', MainPageHandler),
    ('/search', SearchPageHandler),
])
