import jinja2
import os
import webapp2

from google.appengine.ext import ndb


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)


class Person(ndb.Model):
    fio = ndb.StringProperty()
    date_registered = ndb.DateTimeProperty(auto_now_add=True)


class MainPageHandler(webapp2.RequestHandler):
    def get(self):
        template = JINJA_ENVIRONMENT.get_template('templates/main.html')

        # Read from Datastore.
        persons = Person.query().fetch()

        response = template.render(persons=persons)

        self.response.headers['Content-Type'] = 'text/html'
        self.response.write(response)

    def post(self):
        fio = self.request.get('fio')

        person = Person()
        person.fio = fio
        person.put()

        self.redirect('/')


class SearchPageHandler(webapp2.RequestHandler):
    def post(self):
        template = JINJA_ENVIRONMENT.get_template('templates/main.html')

        # Read from Datastore.
        persons = Person.query(Person.fio==self.request.get('fio')).fetch()

        response = template.render(persons=persons)

        self.response.headers['Content-Type'] = 'text/html'
        self.response.write(response)

